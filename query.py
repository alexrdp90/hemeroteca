# -*- coding: utf-8 -*-
from pattern.web import URL, Document, plaintext
from oshelpers import chdir, backdir, opendefault
import re, os

class RecordPDF(object):
    """
    This class represents the PDF of a record
    RecordPDF.openreader opens the PDF with the default program
    """
    def __init__(self, link, name):
        self.link = link
        self.filename = name
        
    def download(self):
        url = URL(self.link)
        f = open(self.filename, 'wb')
        f.write(url.download(cached=False))
        f.close()
        
    def exists(self):
        return os.path.isfile(self.filename)
    
    def openreader(self):
        """
        Returns false if an error occurred
        """
        res = True
        chdir('pdf')
        try:
            if not self.exists():
                self.download()
            opendefault(self.filename)
        except:
            res = False
        finally:
            backdir() 
        return res


class Query(object):
    """
    This class represents a query to 'bnesearch'
    Query.fetch returns all the detail links of the search
    """
    def __init__(self, titles, countries, years):
        self.createquerystring()
        self.querystring['tituloH'] = '|'.join(titles)
        self.querystring['sedeH'] = '|'.join(countries)
        self.querystring['anhoH'] = '|'.join(map(str, years))

    def createquerystring(self):
        self.querystring = {'numfields':'3', 'fechaHsearchtype':'0', 'pageSize':'50'}
        self.querystring['advanced'] = self.querystring['abreviada'] = 'true'
        self.querystring['field1Op'] = self.querystring['field2Op'] = 'AND'
        
    def fetchresultpage(self, index):
        self.querystring['pageNumber'] = str(index)
        url = URL(string='http://bdh.bne.es/bnesearch/HemerotecaSearch.do',
                  method='post', query=self.querystring)
        return ResultPage(url)
    
    def fetch(self):
        validlinks = []
        index = 1
        resultpage = self.fetchresultpage(index)
        while resultpage.isvalid():
            validlinks += resultpage.getlinks()
            index += 1
            resultpage = self.fetchresultpage(index)
        return validlinks


class ResultPage(object):
    """
    This class represents a result page fetched by Query#fetchresultpage
    ResultPage.getlinks returns all the detail links
    ResultPage.isvalid returns False if the page has not detail links
    """
    def __init__(self, url):
        dom = Document(url.download(cached=False))
        self.valid = not dom.by_class('msg')
        self.validlinks = []
        
        # WARNING: This page does not contain valid results anymore.
        # bdh.bne.es has changed the URLs and post parameters and
        # the whole application does not return any search result.
        if self.valid:
            links = [a.attributes['href'] for a in dom.by_tag('a')]
            map(self.processlink, links)
    def processlink(self, link):
        if link.startswith('javascript:sendMail'):
            match = re.search(r'(http://bdh.bne.es/bnesearch/detalle/hemeroteca/\d+)', link)
            if match: self.validlinks.append(match.group(1))
    def getlinks(self):
        return self.validlinks
    def isvalid(self):
        return self.valid


class DetailPage(object):
    def __init__(self, link):
        url = URL(link)
        dom = Document(url.download(cached=False))
        self.keys = [plaintext(x.content) for x in dom.by_class('dato')]
        self.values = [plaintext(x.content) for x in dom.by_class('valor')]
        anchorpdf = dom.by_class('identificador')[0]
        self.linkpdf = anchorpdf.attributes['href'].replace(' ', '%20')
    def __iter__(self):
        return zip(self.keys, self.values).__iter__()
