import os
import sys

def chdir(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)
    os.chdir(directory)

def backdir():
    os.chdir('..')

def opendefault(filename):
    if sys.platform.startswith('win'):
        os.system('start ' + filename)
    elif sys.platform.startswith('linux'):
        os.system('open ' + filename)
    else:
        os.system('open ' + filename)
