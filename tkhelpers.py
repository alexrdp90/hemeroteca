# -*- coding: utf-8 -*-

import Tkinter as tk
import sys


class MainForm(tk.Tk):
    """
    Base class for the main window
    """
    def __init__(self):
        tk.Tk.__init__(self)
        if sys.platform.startswith('win'):
            self.wm_iconbitmap('bne.ico')


class Form(tk.Toplevel):
    """
    Base class for secondary windows
    """
    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        if sys.platform.startswith('win'):
            self.wm_iconbitmap('bne.ico')


class Checkbox(tk.Checkbutton):
    def __init__(self, master, text):
        self._var = tk.BooleanVar()
        tk.Checkbutton.__init__(self, text=text, variable=self._var)
        
    def checked(self):
        return self._var.get()


class CheckboxList(object):
    def __init__(self, master=None, items=[]):
        self.master = master
        self.checkbuttons = [Checkbox(master, item) for item in items]

    def grid(self, row, column):
        for rowoffset, cb in enumerate(self.checkbuttons):
            cb.grid(row=row + rowoffset, column=column, sticky='nsw')

    def getselected(self):
        checked = [cb['text'] for cb in self.checkbuttons if cb.checked()]
        if len(checked) == 0:
            # If no Checkbutton is selected, then return all
            checked = [cb['text'] for cb in self.checkbuttons]
        return checked


class ScrollableListbox(object):
    """
    Auxiliary Listbox widget with a scrollbar
    """
    def __init__(self, master, *arg, **key):
        self.frame = tk.Frame(master)
        self.yscroll = tk.Scrollbar(self.frame, orient=tk.VERTICAL)
        self.listbox = tk.Listbox(self.frame, yscrollcommand=self.yscroll.set,
                                  *arg, **key)
        self.yscroll['command'] = self.listbox.yview

    def insertitems(self, items):
        self.items = items
        for item in items:
            self.listbox.insert(tk.END, item)

    def clear(self):
        self.items = []
        size = self.listbox.size()
        self.listbox.delete(0, size - 1)

    def getselected(self):
        selection = self.listbox.curselection()
        return self.items[int(selection[0])] if len(selection) else None

    def grid(self, *arg, **key):
        self.frame.grid(*arg, **key)
        self.listbox.grid(row=0, column=0, sticky='nsew')
        self.yscroll.grid(row=0, column=1, sticky='ns')

    def bind(self, sequence, func, add=None):
        return self.listbox.bind(sequence, func, add)
