# -*- coding: utf-8 -*-
from pattern.web import URL
import re

def parseint(i):
    return None if i == '' else int(i)

def convertnonascii(s):
    return unicode(s, 'utf-8')

countries = (u'Cuba', u'Perú', u'Venezuela')
years = (1807, 1808, 1809, 1810, 1811, 1812, 1813, 1814, 1815)

def _getallrecords():
    url = URL('http://bdh.bne.es/bnesearch/js/autocompleteLocal/hemeroteca.js')
    jsfile = url.download(cached=True)
    result = re.search(r'\[((\n|.)+)\]', jsfile)
    listofrecords = result.group(1)
    records = re.findall(r'\[(.+?,.+?,.+?)\]', listofrecords)
    return records

def _processrecord(record):
    # record = "Lince, El (Habana)","Cuba","1811"
    m = re.match(r'"(.*)","(.*)","(.*)"', record)
    title = convertnonascii(m.group(1))
    country = convertnonascii(m.group(2))
    year = parseint(m.group(3))
    return title, country, year

def _getavailabletitles():
    availabletitles = set()
    for record in _getallrecords():
        title, country, year = _processrecord(record)
        if country in countries and year in years:
            availabletitles.add(title)
    return list(availabletitles)

titles = _getavailabletitles()
