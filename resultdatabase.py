# -*- coding: utf-8 -*-
from pattern.db import ALL, Database, field, pk
from query import DetailPage, RecordPDF
from oshelpers import chdir, backdir


_maptitles = {
    u'Título' : 'titulo',
    u'Datos de edición' : 'datos',
    u'Tipo de Documento' : 'tipo',
    u'Descripción' : 'descripcion',
    u'Descripción física' : 'descripcion_fisica',
    u'CDU' : 'cdu',
    u'ISSN' : 'issn',
    u'Materia' : 'materia'
}

_maprecords = {
    u'Fecha del ejemplar' : 'fecha',
    u'Número de ejemplar' : 'numero'
}

class DatabaseProcessor(object):
    def __init__(self, dbname):
        chdir('db')
        self.db = Database(dbname, type='sqlite')
        backdir()
        self.processedtitles = []
        self.db.create('titulos', fields=[
            pk(name='id'),
            field('titulo'),
            field('datos'),
            field('tipo'),
            field('descripcion', 'text'),
            field('descripcion_fisica'),
            field('cdu'),
            field('issn'),
            field('materia')
        ])
        self.db.create('ejemplares', fields=[
            pk(name='id'),
            field('idtitulo', 'integer'),
            field('fecha'),
            field('numero'),
            field('url')
        ])
    def processtitle(self, dicctitles):        
        title = dicctitles['titulo']
        if title in self.processedtitles:
            index = self.processedtitles.index(title) + 1
        else:
            self.db.titulos.append(dicctitles)
            self.processedtitles.append(title)
            index = self.db.titulos.count()
        return index
    def processdetaillink(self, link):
        detailpage = DetailPage(link)
        dicctitles = {}
        diccrecords = {'url' : detailpage.linkpdf}
        for key, value in detailpage:
            if key in _maptitles:
                dicctitles[_maptitles[key]] = value
            elif key in _maprecords:
                diccrecords[_maprecords[key]] = value
        index = self.processtitle(dicctitles)
        diccrecords['idtitulo'] = index
        self.db.ejemplares.append(diccrecords)
    def disconnect(self):
        self.db.disconnect()

class ResultDatabase(object):
    def __init__(self, dbname):
        chdir('db')
        self.db = Database(dbname, type='sqlite')
        backdir()
    def getrecords(self, idtitle):
        try:
            titlename = self.gettitle(idtitle).titlename
            rows = self.db.ejemplares.filter(ALL, {'idtitulo':idtitle})
            return [Record(row, titlename) for row in rows]
        except:
            return []
    def gettitle(self, idtitle):
        try: return Title(self.db.titulos.filter(ALL, {'id':idtitle})[0])
        except: return []
    def gettitles(self):
        try: return [Title(row) for row in self.db.titulos.rows()]
        except: return []

class Record(object):
    def __init__(self, row, titlename):
        self.id = row[0]
        self.idtitle = row[1]
        self.date = row[2]
        self.number = row[3] 
        self.link = row[4]
        self.titlename = titlename
    def __repr__(self):
        result = self.date
        if self.number: result += '- Num %s' % self.number
        return result
    def openpdf(self):
        pdfname = '%s_%s.pdf' % (self.titlename.replace(' ', '.'), self.date.replace('/', '-'))
        return RecordPDF(self.link, pdfname).openreader()

class Title(object):
    def __init__(self, row):
        self.id = row[0]
        self.titlename = row[1]
        self.fields = row[2:]
    def __repr__(self):
        return self.titlename
