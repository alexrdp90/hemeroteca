# -*- coding: utf-8 -*-

import threading
import datetime
import Queue
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import ttk

import resultdatabase
import tkhelpers
import searchoptions
import query
import resultdatabase


class Application(tkhelpers.MainForm):

    def __init__(self):
        tkhelpers.MainForm.__init__(self)
        self.title(u'Búsqueda en http://bdh.bne.es/')
        
        menubar = tk.Menu(self)
        menubar.add_command(label=u'Abrir', command=self.openhandler)
        menubar.add_command(label=u'Salir', command=self.quithandler)
        self.config(menu=menubar)
        
        self.titlelabel = tk.Label(self, text=u'Seleccione los títulos')
        self.countrylabel = tk.Label(self, text=u'Seleccione los lugares de publicación')
        self.yearlabel = tk.Label(self, text=u'Seleccione los años')

        self.listboxtitles = tkhelpers.CheckboxList(self, searchoptions.titles)
        self.listboxcountries = tkhelpers.CheckboxList(self, searchoptions.countries)
        self.listboxyears = tkhelpers.CheckboxList(self, searchoptions.years)
        
        self.searchbutton = tk.Button(self, text=u'Buscar', width=10,
                                      command=self.searchhandler)
        self.quitbutton = tk.Button(self, text=u'Salir', width=10,
                                    command=self.quithandler)
        
        self.gridwidgets()

    def gridwidgets(self):
        self.titlelabel.grid(row=0, column=0)
        self.countrylabel.grid(row=0, column=1)
        self.yearlabel.grid(row=0, column=2)
        
        self.listboxtitles.grid(row=1, column=0)
        self.listboxcountries.grid(row=1, column=1)
        self.listboxyears.grid(row=1, column=2)
        
        self.searchbutton.grid(row=20, column=1, padx=5, pady=5)
        self.quitbutton.grid(row=20, column=2, padx=5, pady=5)

    def searchhandler(self):
        titles = self.listboxtitles.getselected()
        countries = self.listboxcountries.getselected()
        years = self.listboxyears.getselected()
        
        ProgressForm(self, query.Query(titles, countries, years))

    def quithandler(self):
        close = tkMessageBox.askokcancel(u'Salir', u'¿Realmente quieres salir?')
        if close:
            self.destroy()

    def openhandler(self):
        dbname = tkFileDialog.askopenfilename(initialdir='./db')
        if dbname:
            ResultForm(self, dbname)


class ProgressForm(tkhelpers.Form):
    def __init__(self, master, query):
        tkhelpers.Form.__init__(self, master)
        self.title(u'En proceso')
        
        self.label = tk.Label(self, text=u'Calculando el nº de resultados...')
        self.progressbar = ttk.Progressbar(self)
        self.label.grid(row=0, column=0, pady=10)
        self.progressbar.grid(row=1, column=0, padx=50, pady=10)
        
        strnow = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
        self.dbname = 'busq_{}.db'.format(strnow)
        self.queue = Queue.Queue()
        self.thread = ThreadedProgress(query, self.dbname, self.queue)
        self.thread.start()
        self.periodiccall()
        
    def periodiccall(self):
        if self.thread.isAlive():
            while self.queue.qsize():
                self.processincoming()
            self.master.after(100, self.periodiccall)
        else:
            ResultForm(self.master, self.dbname)
            self.destroy()
    
    def processincoming(self):
        try:
            val = self.queue.get(0)
            if isinstance(val, int):
                self.label['text'] = u'Progreso para un total de %s resultados' % total
                self.total = total
            else:
                self.progressbar.step(100. / self.total)
        except Queue.Empty:
            pass


class ThreadedProgress(threading.Thread):
    def __init__(self, query, dbname, queue):
        threading.Thread.__init__(self)
        self.query = query
        self.dbname = dbname
        self.queue = queue

    def run(self):
        databaseprocessor = resultdatabase.DatabaseProcessor(self.dbname)
        validlinks = self.query.fetch()
        print validlinks
        self.queue.put(len(validlinks))
        for link in validlinks:
            databaseprocessor.processdetaillink(link)
            self.queue.put("")
        databaseprocessor.disconnect()


class ResultForm(tkhelpers.Form):
    def __init__(self, master, dbname):
        tkhelpers.Form.__init__(self, master)
        self.title(u'Resultado de la búsqueda')
        
        self.databaseresult = resultdatabase.ResultDatabase(dbname)
        self.titles = self.databaseresult.gettitles()
        
        self.titleslabel = tk.Label(self, text=u'Títulos')
        self.listboxtitles = tkhelpers.ScrollableListbox(self, width=30, height=10)
        self.listboxtitles.insertitems(self.titles)
        self.listboxtitles.bind('<<ListboxSelect>>', self.refreshhandler)
        self.descriptionbutton = tk.Button(self, text=u'Mostrar descripción', command=self.showdescription)
        
        self.recordslabel = tk.Label(self, text=u'Ejemplares')
        self.listboxrecords = tkhelpers.ScrollableListbox(self, width=30, height=10)
        self.openbutton = tk.Button(self, text=u'Abrir PDF', command=self.openpdf)
        
        self.gridwidgets()
        
    def gridwidgets(self):
        self.titleslabel.grid(row=0, column=0, pady=5)
        self.listboxtitles.grid(row=1, column=0, padx=5, pady=5)
        self.descriptionbutton.grid(row=2, column=0, pady=5)
        
        self.recordslabel.grid(row=0, column=1, pady=5)
        self.listboxrecords.grid(row=1, column=1, padx=5, pady=5)
        self.openbutton.grid(row=2, column=1, pady=5)
    
    def refreshhandler(self, event):
        title = self.listboxtitles.getselected()
        if title:
            records = self.databaseresult.getrecords(int(title.id))
            self.listboxrecords.clear()
            self.listboxrecords.insertitems(records)
        else:
            tkMessageBox.showinfo('Info', u'Seleccione un título para ver los ejemplares disponibles')
    
    def showdescription(self):
        title = self.listboxtitles.getselected()
        if title:
            header = title.titlename
            text = reduce(lambda x, y: x + '\n' + y, filter(lambda x: x, title.fields))
        else:
            header, text = 'Info', u'Seleccione un título para ver su descripción'
        tkMessageBox.showinfo(header, text)
    
    def openpdf(self):
        record = self.listboxrecords.getselected()
        if record:
            res = record.openpdf()
            if not res: tkMessageBox.showerror('Error', u'Hubo un error al conectar con el servidor.\nPor favor, intente descargar el PDF más tarde.')
        else:
            tkMessageBox.showinfo('Info', u'Seleccione un ejemplar para abrir el PDF')


if __name__ == '__main__':
    app = Application()
    app.mainloop()
